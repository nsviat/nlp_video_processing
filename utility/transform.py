import cv2

from moviepy.editor import *


def video2audio(path):
    return VideoFileClip(path).audio


def video2frames(path):
    cap = cv2.VideoCapture(path)
    video_length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) - 1
    count = 0
    frames = []
    while cap.isOpened():
        ret, frame = cap.read()
        frames.append(frame)
        count = count + 1

        if count > (video_length - 1):
            cap.release()
            break
    return frames
