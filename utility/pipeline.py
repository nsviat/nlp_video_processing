class Pipeline(object):
    def __init__(self):
        self.steps = []

    def add_step(self, reco):
        self.steps.append(reco.run)

    def run(self, arg):
        pipeline = self._connect(self.steps)
        pipeline(arg)

    def _connect(self, funcs):
        def wrapper(*args, **kwargs):
            data_out = funcs[0](*args, **kwargs)
            for func in funcs[1:]:
                data_out = func(data_out)
            return data_out

        return wrapper
