from third import recognize_emotion


class RecognitionEmotion:

    def run(self, imgs):
        emotions = []
        for img in imgs:
            emo = recognize_emotion(img)
            if emo is not None:
                emotions.append(emo)
        return emotions
