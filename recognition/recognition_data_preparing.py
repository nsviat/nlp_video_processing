from utility.transform import video2audio, video2frames


class RecognitionDataPreparing:

    def run(self, path):
        audio = video2audio(path)
        frames = video2frames(path)

        return audio, frames
