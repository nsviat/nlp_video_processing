from third import EmotionRecognizer


class RecognitionTextEmotion:

    def run(self, txt):
        er = EmotionRecognizer()
        return er.recognize_emotion(txt)
