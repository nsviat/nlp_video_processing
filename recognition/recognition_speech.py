import speech_recognition as sr

r = sr.Recognizer()


def load_audio(path):
    audio = sr.AudioFile(path)
    return audio


def record_audio(audio, duration=None):
    with audio as source:
        audio_records = r.record(source, duration)
        return audio_records


def recognize_audio(audio_records):
    text = r.recognize_google(audio_records)
    return text


def audio_to_text(path, duration=None):
    audio = load_audio(path)
    audio_records = record_audio(audio, duration)
    text = recognize_audio(audio_records)
    return text


class RecognitionSpeech:

    def run(self, path):
        return audio_to_text(path)

