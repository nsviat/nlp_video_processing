import time

import numpy

from recognition.recognition_data_preparing import RecognitionDataPreparing
from recognition.recognition_emotion import RecognitionEmotion
from recognition.recognition_speech import RecognitionSpeech
from recognition.recognition_txtemotion import RecognitionTextEmotion
from utility.pipeline import Pipeline

TEMP_AUDIO = r"E:\NLRecognizer\recognition\meta\audio.wav"


def run_all(path):
    audio, frames = RecognitionDataPreparing().run(path)
    audio.write_audiofile(TEMP_AUDIO)
    time.sleep(30)

    audio_pipeline = Pipeline()
    audio_pipeline.add_step(RecognitionSpeech())
    text = RecognitionSpeech().run(TEMP_AUDIO)
    audio_pipeline.add_step(RecognitionTextEmotion())
    text_emotions = RecognitionTextEmotion().run(text.split(' '))

    video_pipeline = Pipeline()
    video_pipeline.add_step(RecognitionEmotion())
    emotions = RecognitionEmotion().run(frames)

    print(text)
    print(text_emotions)
    print(emotions)
    numpy.histogram(text_emotions)
    return text, text_emotions, emotions


def commutate(text_emotion, emotions):
    mode = max(set(emotions), key=emotions.count)
    if emotions.count(mode) > len(emotions) / 2.5:
        return mode
    else:
        return text_emotion

