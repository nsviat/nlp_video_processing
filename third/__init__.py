from keras.preprocessing.image import img_to_array
import imutils
import cv2
from keras.models import load_model
import numpy as np
import itertools
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier

from third.isear_loader import IsearLoader

ROOT = r"C:/Users/Pavlo/source/repos/NLRecognizer/third/"

detection_model_path = 'E:/NLRecognizer/third/haarcascade_files/haarcascade_frontalface_default.xml'
emotion_model_path = 'E:/NLRecognizer/third/models/_mini_XCEPTION.102-0.66.hdf5'
EMOTIONS = ["angry", "disgust", "scared", "happy", "sad", "surprised",
            "neutral"]
face_detection = cv2.CascadeClassifier(detection_model_path)
emotion_classifier = load_model(emotion_model_path, compile=False)


def recognize_emotion(image):
    try:
        image = imutils.resize(image, width=300)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        faces = face_detection.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30),
                                                flags=cv2.CASCADE_SCALE_IMAGE)

        if len(faces) > 0:
            faces = sorted(faces, reverse=True,
                           key=lambda x: (x[2] - x[0]) * (x[3] - x[1]))[0]
            (fX, fY, fW, fH) = faces

            roi = gray[fY:fY + fH, fX:fX + fW]
            roi = cv2.resize(roi, (64, 64))
            roi = roi.astype("float") / 255.0
            roi = img_to_array(roi)
            roi = np.expand_dims(roi, axis=0)

            preds = emotion_classifier.predict(roi)[0]
            label = EMOTIONS[preds.argmax()]
            return label

        else:

            return None
    except:
        return None


class EmotionRecognizer:
    def __init__(self):
        data = ['TEMPER', 'TROPHO']
        target = ['EMOT']
        loader = IsearLoader(data, target)
        dataset = loader.load_isear(r'E:\NLRecognizer\third\isear.csv')

        text_data_set = dataset.get_freetext_content()
        target_set = dataset.get_target()
        target_chain = itertools.chain(*target_set)
        target_data = list(target_chain)
        self.text_clf = Pipeline([('vect', CountVectorizer()),
                                  ('tfidf', TfidfTransformer()),
                                  ('clf', SGDClassifier(loss='hinge', penalty='l2',
                                                        alpha=1e-3, max_iter=5, random_state=42)),
                                  ])
        self.text_clf.fit(text_data_set, target_data)

    def recognize_emotion(self, sentence):
        emotions = ["happy", "scared", "angry", "sad", "disgust", "surprised",
                    "neutral"]
        res = self.text_clf.predict(sentence)
        res = list(res)

        return emotions
