from tkinter import *
from tkinter import filedialog, messagebox, ttk

from recognition.commutation import commutate, run_all


def load_video_event(event):
    global file
    filename = filedialog.askopenfilename()
    if not filename.endswith('.mp4'):
        messagebox.showinfo("Visualizer error", "Filetype must be a .mp4")
    else:
        file = filename


def process_video_event(event):
    global text
    global emotion
    global vid_emotions
    global pb
    global lRes

    try:
        file
    except NameError:
        messagebox.showinfo("Visualizer error", "Video is not loaded")
        return
    if file is None:
        messagebox.showinfo("Visualizer error", "Video is not loaded")
        return
    else:
        pb["maximum"] = 100
        pb.start(50)
        text, text_emotion, emotions = run_all(file)
        emotion = commutate(text_emotion, emotions)
        vid_emotions = set(emotions)
        lRes.insert(END, text)
        lRes.insert(END, '\n'+"EMOTIONS FROM VIDEO: " + ",".join(vid_emotions))
        lRes.insert(END, '\n' + "EMOTION OF TEXT: " + text_emotion)
        lRes.insert(END, '\n' + "EMOTIONAL MODE: " + emotion)
        pb.stop()


root = Tk()
root.geometry('400x400')
root.title('NLRecognizer')


emotion = ""
bLoad = Button(root, text="LoadVideo")
bProc = Button(root, text="Process")
pb = ttk.Progressbar(root, orient="horizontal", length=200, mode="determinate")
lRes = Text(bg='white', fg='black', width=100, height=40)

bLoad.bind('<Button-1>', load_video_event)
bProc.bind('<Button-1>', process_video_event)

pb.pack()
bLoad.pack(side="left")
bProc.pack(side="left")
lRes.pack()
root.mainloop()
